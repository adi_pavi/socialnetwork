package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Event extends Entity<Long> {
    private Utilizator owner;
    private String description;
    private LocalDateTime eventDate;
    private List<Utilizator> subscribers;

    public Event(Utilizator owner, String description, List<Utilizator> subscribers) {
        this.owner = owner;
        this.description = description;
        this.eventDate = LocalDateTime.now();
        this.subscribers = subscribers;
    }

    public Event(Utilizator owner, String description, List<Utilizator> subscribers, LocalDateTime eventDate) {
        this.owner = owner;
        this.description = description;
        this.eventDate = eventDate;
        this.subscribers = subscribers;
    }

    public Utilizator getOwner() {return owner;}
    public String getDescription() {return description;}
    public LocalDateTime getEventDate() {return eventDate;}
    public List<Utilizator> getSubscribers() {return subscribers;}

    @Override
    public String toString(){
        List<Long>lst=new ArrayList<>();
        for(Utilizator ut:subscribers)
            lst.add(ut.getId());
        String msg=""+this.getId()+";"+owner.getId()+";"+lst+";"+eventDate+";"+description;
        return msg;
    }
}
