package socialnetwork.ui;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import socialnetwork.domain.Utilizator;

import javafx.scene.image.ImageView;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class ActionButtonTableCell <S> extends TableCell<S, Button> {

    private final Button actionButton;
    private BiConsumer<S, TableCell> formatFct;

    public ActionButtonTableCell(String label, String iconName, Function<S, S> function, BiConsumer <S, TableCell> formatFct) {
        this.getStyleClass().add("action-button-table-cell");
        ImageView icon = null;
        if (iconName != null)
            icon = new ImageView(iconName);
        this.actionButton = new Button(label, icon);
        this.actionButton.setOnAction((ActionEvent e) -> {
            function.apply(getCurrentItem());
        });
        this.actionButton.setMaxWidth(Double.MAX_VALUE);
        this.formatFct = formatFct;
        if(label == null || label.isEmpty()){
            this.actionButton.setStyle("-fx-background-color: transparent;");
        }
    }

    public S getCurrentItem() {
        return (S) getTableView().getItems().get(getIndex());
    }

    public static <S> Callback<TableColumn<S, Button>, TableCell<S, Button>> forTableColumn(String label, String iconName, Function<S, S> function, BiConsumer<S, TableCell> formatFct) {
        return param -> new ActionButtonTableCell<>(label, iconName, function, formatFct);
    }

    @Override
    public void updateItem(Button item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setDisable(false);
            setGraphic(null);
        } else {
            setGraphic(actionButton);
            if (formatFct != null) {
                formatFct.accept(getCurrentItem(), this);
            }
        }
    }
}
