package socialnetwork.service;

import socialnetwork.domain.Event;
import socialnetwork.domain.Subscription;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class EventService {
    private PagingRepository<Long, Event> repoEvents;
    private PagingRepository<Tuple<Long,Long>, Subscription> repoSubs;
    private PagingRepository<Long, Utilizator> repoUtilizator;
    public static final int REMINDER_DAYS = 3;

    public EventService(PagingRepository<Long, Event> repoEvents, PagingRepository<Tuple<Long,Long>, Subscription> repoSubs, PagingRepository<Long, Utilizator> repoUtilizator) {
        this.repoEvents = repoEvents;
        this.repoSubs = repoSubs;
        this.repoUtilizator = repoUtilizator;
    }
    public Iterable<Event> getAll()
    {
        return repoEvents.findAll();
    }
    public Page<Event> getAll(Pageable pageable, boolean refresh) { return repoEvents.findAll(pageable, refresh); }
    public Event getOne(Long id){return repoEvents.findOne(id);}
    public Subscription getSubscription(Long id, Long userId) {return repoSubs.findOne(new Tuple<>(id, userId));}
    public Iterable<Event> getAllSubscribedDue(Long id) {
        return getAllSubscribedDue(id, REMINDER_DAYS);
    }
    public Iterable<Event> getAllSubscribedDue(Long id, int days) {
        List<Event> eventsDue = new ArrayList<>();
        LocalDateTime now = LocalDate.now().atStartOfDay();
        LocalDateTime due = now.plusDays(days);
        repoEvents.findAll().forEach(x->
        {
            if(x.getSubscribers().stream().anyMatch(u->u.getId() == id) &&
                    (x.getEventDate().isEqual(now) || x.getEventDate().isAfter(now)) &&
                    x.getEventDate().isBefore(due)) {
                eventsDue.add(x);
            }
        });
        return eventsDue.stream().sorted(Comparator.comparing(Event::getEventDate)).collect(Collectors.toList());
    }
    public void delete(Long id) {repoEvents.delete(id);}

    public void toggleSubscription(Long idEvent, Long idUserLogged) {
        Tuple<Long, Long> subId = new Tuple<>(idEvent, idUserLogged);
        Subscription sub = repoSubs.findOne(subId);
        if (sub == null) {
            sub = new Subscription();
            sub.setId(subId);
            repoSubs.save(sub);
        } else {
            repoSubs.delete(subId);
        }
    }
    public void addEvent(Event event) {repoEvents.save(event);}
}
