package socialnetwork.repository.paging;

import socialnetwork.repository.Repository;
import socialnetwork.domain.Entity;

public interface PagingRepository<ID, E extends Entity<ID>> extends Repository<ID, E> {
    Page<E> findAll(Pageable pageable);   // Pageable e un fel de paginator
    Page<E> findAll(Pageable pageable, boolean refresh);   // Pageable e un fel de paginator
}
