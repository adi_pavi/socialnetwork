package socialnetwork.repository.database;

import org.postgresql.util.PSQLException;
import socialnetwork.domain.Event;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class EventDbRepository implements PagingRepository<Long, Event> {
    private String url;
    private String username;
    private String password;

    public EventDbRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public Event findOne(Long aLong) {
        String q = "SELECT * FROM events WHERE id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(q);
            statement.setInt(1, aLong.intValue());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Long id_ev = resultSet.getLong("id");
                Long id_owner = resultSet.getLong("id_owner");
                String description = resultSet.getString("description");
                LocalDateTime eventDate = LocalDateTime.parse(resultSet.getString("event_date"));

                Utilizator owner = new Utilizator();

                String q1 = "SELECT * from users WHERE id = ?";
                PreparedStatement statement1 = connection.prepareStatement(q1);
                statement1.setInt(1, id_owner.intValue());
                ResultSet resultSet1 = statement1.executeQuery();
                if(resultSet1.next()) {
                    Long id = resultSet1.getLong("id");
                    String firstName = resultSet1.getString("nume");
                    String lastName = resultSet1.getString("prenume");

                    owner.setFirstName(firstName);
                    owner.setLastName(lastName);
                    owner.setId(id);
                }

                // imi iau subscriberii pentru eveniment
                List<Utilizator> subscribers = new ArrayList<>();
                String q2 = "SELECT * from subscriptions s JOIN users u ON (s.id_user = u.id) where s.id_ev = ?";
                PreparedStatement statement2 = connection.prepareStatement(q2);
                statement2.setInt(1,id_ev.intValue());
                ResultSet resultSet2 = statement2.executeQuery();
                while(resultSet2.next())
                {
                    Long id = resultSet2.getLong("id");
                    String firstName = resultSet2.getString("nume");
                    String lastName = resultSet2.getString("prenume");

                    Utilizator user = new Utilizator(firstName, lastName);
                    user.setId(id);
                    subscribers.add(user);
                }

                Event event = new Event(owner, description, subscribers, eventDate);
                event.setId(id_ev);
                return event;
            }
        } catch (PSQLException e) {
            System.out.println(e);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Iterable<Event> findAll() {
        String q = "SELECT * FROM events";
        List<Event> lista_ev = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(q);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id_ev = resultSet.getLong("id");
                Long id_owner = resultSet.getLong("id_owner");
                String description = resultSet.getString("description");
                LocalDateTime eventDate = LocalDateTime.parse(resultSet.getString("event_date"));

                Utilizator owner = new Utilizator();

                String q1 = "SELECT * from users WHERE id = ?";
                PreparedStatement statement1 = connection.prepareStatement(q1);
                statement1.setInt(1, id_owner.intValue());
                ResultSet resultSet1 = statement1.executeQuery();
                if(resultSet1.next()) {
                    Long id = resultSet1.getLong("id");
                    String firstName = resultSet1.getString("nume");
                    String lastName = resultSet1.getString("prenume");

                    owner.setFirstName(firstName);
                    owner.setLastName(lastName);
                    owner.setId(id);
                }

                // imi iau subscriberii pentru eveniment
                List<Utilizator> subscribers = new ArrayList<>();
                String q2 = "SELECT * from subscriptions s JOIN users u ON (s.id_user = u.id) where s.id_ev = ?";
                PreparedStatement statement2 = connection.prepareStatement(q2);
                statement2.setInt(1,id_ev.intValue());
                ResultSet resultSet2 = statement2.executeQuery();
                while(resultSet2.next())
                {
                    Long id = resultSet2.getLong("id");
                    String firstName = resultSet2.getString("nume");
                    String lastName = resultSet2.getString("prenume");

                    Utilizator user = new Utilizator(firstName, lastName);
                    user.setId(id);
                    subscribers.add(user);
                }

                Event event = new Event(owner, description, subscribers, eventDate);
                event.setId(id_ev);
                lista_ev.add(event);
            }
            return lista_ev;
        } catch (PSQLException e) {
            System.out.println(e);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Event save(Event entity) {
        String q = "INSERT INTO events values(?,?,?,?)";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(q);
            statement.setInt(1,entity.getId().intValue());
            statement.setInt(2,entity.getOwner().getId().intValue());
            statement.setString(3,entity.getDescription());
            statement.setString(4,entity.getEventDate().toString());
            statement.executeUpdate();

            // We are NOT saving the subscribers!!!
        }catch (PSQLException e) {
            System.out.println(e);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return entity;
    }

    @Override
    public Event delete(Long aLong) {
        String q = "DELETE FROM events WHERE id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(q);
            statement.setInt(1,aLong.intValue());
            statement.executeUpdate();
        }catch (PSQLException e) {
            System.out.println(e);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Event update(Event entity) {
        return null;
    }

    private Paginator<Event> paginator;

    @Override
    public Page<Event> findAll(Pageable pageable) {
        paginator = new Paginator<>(pageable, findAll());
        return paginator.paginate();
    }

    @Override
    public Page<Event> findAll(Pageable pageable, boolean refresh) {
        if (refresh) {
            return findAll(pageable);
        }
        paginator.setPageable(pageable);
        return paginator.paginate();
    }
}
