package socialnetwork.repository.database;

import org.postgresql.util.PSQLException;
import socialnetwork.domain.Request;
import socialnetwork.domain.Subscription;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class SubscriptionDbRepository implements PagingRepository<Tuple<Long,Long>, Subscription> {
    private String url;
    private String username;
    private String password;

    public SubscriptionDbRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public Subscription findOne(Tuple<Long, Long> id) {
        String q = "SELECT * FROM subscriptions where id_ev = ? and id_user =?";
        try (Connection connection = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = connection.prepareStatement(q);
            statement.setInt(1,id.getLeft().intValue());
            statement.setInt(2,id.getRight().intValue());
            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next()) {
                Long id_ev = resultSet.getLong("id_ev");
                Long id_user = resultSet.getLong("id_user");

                Subscription sub = new Subscription();
                sub.setId(new Tuple(id_ev, id_user));
                return sub;
            }
            return null;
        } catch (PSQLException e) {
            System.out.println(e);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Iterable<Subscription> findAll() {
        Set<Subscription> subs = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * from subscriptions");
            ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id_ev = resultSet.getLong("id_ev");
                Long id_user = resultSet.getLong("id_user");
                Subscription sub = new Subscription();
                sub.setId(new Tuple(id_ev, id_user));
                subs.add(sub);
            }
            return subs;
        } catch (PSQLException e) {
            System.out.println(e);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return subs;
    }

    @Override
    public Subscription save(Subscription entity) {
        String q = "INSERT INTO subscriptions (id_ev, id_user) VALUES (?,?)";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(q);
            statement.setInt(1,entity.getId().getLeft().intValue());
            statement.setInt(2,entity.getId().getRight().intValue());
            statement.executeUpdate();
        } catch (PSQLException e) {
            System.out.println(e);
        } catch(SQLException e) {
            System.out.println(e);
        }
        return entity;
    }

    @Override
    public Subscription delete(Tuple<Long, Long> id) {
        Subscription ret=findOne(id);
        if (ret==null)
            throw new IllegalArgumentException("nu sunt aceste id uri");

        String q = "DELETE from subscriptions where id_ev=? and id_user = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(q);
            statement.setInt(1,id.getLeft().intValue());
            statement.setInt(2,id.getRight().intValue());
            statement.executeUpdate();
        } catch (PSQLException e) {
            System.out.println(e);
        } catch(SQLException e) {
            System.out.println(e);
        }
        return ret;
    }

    @Override
    public Subscription update(Subscription entity) {
        return null;
    }

    private Paginator<Subscription> paginator;

    @Override
    public Page<Subscription> findAll(Pageable pageable) {
        paginator = new Paginator<>(pageable, findAll());
        return paginator.paginate();
    }

    @Override
    public Page<Subscription> findAll(Pageable pageable, boolean refresh) {
        if (refresh) {
            return findAll(pageable);
        }
        paginator.setPageable(pageable);
        return paginator.paginate();
    }
}
