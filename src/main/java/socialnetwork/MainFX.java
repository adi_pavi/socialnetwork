package socialnetwork;

import controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.repository.database.*;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.service.*;


public class MainFX extends Application {
    private UtilizatorService srv=null;
    private PrietenieService srvPrietenie=null;
    private RequestService srvRequest=null;
    private MessageService srvMessage=null;
    private EventService srvEvent=null;

    public void init_srv()
    {
        // System.out.println("dada");
        String fileName    = ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
        //String prieteniName=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.prietenii");
        //String fileName="data/users.csv";
        String prieteniName = "data/prietenii.csv";
        // System.out.println("READING data from database");
        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username="postgres";
        final String pasword= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");
        PagingRepository<Long,Utilizator> databaseUserRepository =
                new UtilizatorDbRepository(url,username, pasword,  new UtilizatorValidator());

        PagingRepository<Tuple<Long,Long>,Prietenie> databasePrietenieRepository =
                new PrietenieDbRepository(url,username, pasword,  new UtilizatorValidator());

        PagingRepository<Long, Message> databaseMessageRepository =
                new MessageDbRepository(url,username,pasword);

        PagingRepository<Tuple<Long,Long>, Request> databaseRequestRepository=
                new RequestDbRepository(url,username,pasword);

        PagingRepository<Long, Event> databaseEventRepository =
                new EventDbRepository(url, username, pasword);

        PagingRepository<Tuple<Long,Long>, Subscription> databaseSubsRepository =
                new SubscriptionDbRepository(url, username, pasword);

         srv = new UtilizatorService(databaseUserRepository,databasePrietenieRepository,databaseRequestRepository);
         srvPrietenie = new PrietenieService(databaseUserRepository,databasePrietenieRepository,databaseRequestRepository);
         srvMessage = new MessageService(databaseMessageRepository,databaseUserRepository);
         srvRequest = new RequestService(databaseRequestRepository,databaseUserRepository,databasePrietenieRepository);
         srvEvent = new EventService(databaseEventRepository, databaseSubsRepository, databaseUserRepository);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        init_srv();
        FXMLLoader loader = new FXMLLoader();
        // loader.setLocation(getClass().getResource("/Views/Login.fxml"));
        loader.setLocation(getClass().getResource("/Views/Page.fxml"));
        Pane root;
        root = loader.load();

        Controller ctr = loader.getController();
        ctr.initial(srv,srvPrietenie,srvMessage,srvRequest,srvEvent);
        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Program");
        primaryStage.show();
    }

    public static void main(String[] args){
        launch();
    }
}
