package controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EventDTO {
    private String description;
    private LocalDateTime eventDate;
    private Long idOwner;
    private Long idEvent;

    public EventDTO(String description, LocalDateTime eventDate, Long idOwner, Long idEvent) {
        this.description = description;
        this.eventDate = eventDate;
        this.idOwner = idOwner;
        this.idEvent = idEvent;
    }

    public String getDescription() {return description;}
    public String getEventDate() {
        return eventDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
    public Long getIdOwner() {return idOwner;}
    public Long getIdEvent() {return idEvent;}
}
