package controller;

import socialnetwork.service.*;

public abstract class Controller {
    public abstract void initial(UtilizatorService srvU, PrietenieService srvP, MessageService srvMessaj, RequestService srvRequest, EventService srvEvent);
}
