package controller;

import com.google.common.hash.Hashing;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import socialnetwork.domain.*;
import socialnetwork.domain.Event;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.service.*;
import socialnetwork.ui.ActionButtonTableCell;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class PageController extends Controller {
    private Long idUserLogged;
    private UtilizatorService srv;
    private PrietenieService srvPrietenie;
    private MessageService srvMessaj;
    private RequestService srvRquest;
    private EventService srvEvent;
    private boolean isLoggedIn;

    private Integer reminderDays;
    private Integer rowsPerPage = 3;
    private Integer dataRefreshPeriod = 10;
    private Boolean autoRefresh = false;

    @FXML
    private AnchorPane page;
    @FXML
    private AnchorPane page_login;
    @FXML
    private AnchorPane page_content;

    @FXML
    private Label erroruser;
    @FXML
    private Label errorpassword;
    @FXML
    private TextField user_field;
    @FXML
    private PasswordField password_field;

    @FXML
    private Label full_name;
    @FXML
    private Accordion accord;
    @FXML
    private TitledPane friends_messages_pane;
    @FXML
    private TitledPane friend_requests_pane;
    @FXML
    private TitledPane events_pane;
    @FXML
    private TitledPane reports_pane;


    @FXML
    private Pagination friend_list_pagination;
    @FXML
    private TableView friends_list;
    @FXML
    private TableColumn friend_firstName;
    @FXML
    private TableColumn friend_lastName;
    @FXML
    private TableColumn friend_delete;
    @FXML
    private ComboBox friend_name;
    @FXML
    private Label friend_add_err;

    @FXML
    private Pagination message_list_pagination;
    @FXML
    private TableView message_list;
    @FXML
    private TableColumn msg_firstName;
    @FXML
    private TableColumn msg_lastName;
    @FXML
    private TableColumn msg_mesaj;
    @FXML
    private TableColumn msg_reply_to;
    @FXML
    private TableColumn msg_reply_btn;
    @FXML
    private TextField messageToSend;

    @FXML
    private Pagination request_list_pagination;
    @FXML
    private TableView request_list;
    @FXML
    private TableColumn req_firstName;
    @FXML
    private TableColumn req_lastName;
    @FXML
    private TableColumn req_status;
    @FXML
    private TableColumn req_date;
    @FXML
    private TableColumn req_accept;
    @FXML
    private TableColumn req_reject;
    @FXML
    private TableColumn req_withdraw;

    @FXML
    private DatePicker rap_de_la;
    @FXML
    private DatePicker rap_pana_la;
    @FXML
    private ComboBox rap_friend;

    @FXML
    private Pagination event_list_pagination;
    @FXML
    private Pagination due_event_list_pagination;
    @FXML
    private TableView event_list;
    @FXML
    private TableColumn evt_description;
    @FXML
    private TableColumn evt_date;
    @FXML
    private TableColumn evt_subscribe;
    @FXML
    private TableColumn evt_delete;
    @FXML
    private DatePicker event_date;
    @FXML
    private TextField event_description;
    @FXML
    private Button btn_due_events;

    @FXML
    private TableView due_event_list;
    @FXML
    private TableColumn due_evt_description;
    @FXML
    private TableColumn due_evt_date;

    private final ObservableList<Utilizator> observableList = FXCollections.observableArrayList();
    private final ObservableList<Utilizator> possibleFriendsList = FXCollections.observableArrayList();
    private final ObservableList<ConvDTO> msgList = FXCollections.observableArrayList();
    private final ObservableList<ReqUser> requestList = FXCollections.observableArrayList();
    private final ObservableList<EventDTO> eventList = FXCollections.observableArrayList();
    private final ObservableList<EventDTO> dueEventList = FXCollections.observableArrayList();
    private Utilizator ut;
    private Utilizator selected;

    private boolean _firstRun = true;
    private Timeline dataRefresher;

    @FXML
    public void initialize() {
        initialize_data_refresher();
        SetLoggedIn(0L);
        initialize_friends_list();
        initialize_messages_list();
        initialize_request_list();
        initialize_reports();
        initialize_events();
        accord.setExpandedPane(friends_messages_pane);

        reminderDays = EventService.REMINDER_DAYS;
    }

    private void initialize_data_refresher() {
        dataRefresher = new Timeline(
                new KeyFrame(Duration.seconds(dataRefreshPeriod),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                if (autoRefresh) {
                                    refresh_all();
                                }
                            }
                        }));
        dataRefresher.setCycleCount(Timeline.INDEFINITE);
    }

    private Node createEventPage(int pageIndex) {
        int fromIndex = pageIndex * rowsPerPage;
        int toIndex = Math.min(fromIndex + rowsPerPage, eventList.size());
        if(eventList.isEmpty()) return null;
        event_list.setItems(FXCollections.observableList(eventList.subList(fromIndex, toIndex)));
        return new BorderPane(event_list);
    }

    private Node createDueEventPage(int pageIndex) {
        int fromIndex = pageIndex * rowsPerPage;
        int toIndex = Math.min(fromIndex + rowsPerPage, dueEventList.size());
        if(dueEventList.isEmpty()) return null;
        due_event_list.setItems(FXCollections.observableList(dueEventList.subList(fromIndex, toIndex)));
        return new BorderPane(due_event_list);
    }

    private void initialize_events() {
        // Init paging ...
        // For Basic ...
        // event_list_pagination.setPageFactory(this::createEventPage);
        // For Paginator/PagingRepository
        event_list_pagination.setPageCount(-1);
        event_list_pagination.setMaxPageIndicatorCount(1);
        // Done init paging

        due_event_list_pagination.setPageFactory(this::createDueEventPage);
        event_date.setValue(LocalDate.now());
        evt_delete.setCellFactory(ActionButtonTableCell.<EventDTO>forTableColumn("", "remove-16x16.png", (EventDTO e) -> {
            // System.out.println(e);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Stergere eveniment");
            alert.setHeaderText(e.getDescription() + " " + e.getEventDate());
            alert.setContentText("Doresti sa stergi evenimentul?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                srvEvent.delete(e.getIdEvent());
                refresh_events();
                refresh_dueEvents();
            }
            return e;
        }, (EventDTO e, TableCell cell) -> {
            if (e.getIdOwner() != idUserLogged) {
                cell.setDisable(true);
            } else {
                cell.setDisable(false);
            }
        }));
        evt_subscribe.setCellFactory(ActionButtonTableCell.<EventDTO>forTableColumn("", null, (EventDTO e) -> {
            // System.out.println(e);
            srvEvent.toggleSubscription(e.getIdEvent(), idUserLogged);
            refresh_events();
            refresh_dueEvents();
            return e;
        }, (EventDTO e, TableCell cell) -> {
            Button btn = (Button)cell.getGraphic();
            if(btn != null) {
                Event ev = srvEvent.getOne(e.getIdEvent());
                ImageView icon = null;
                if (!ev.getSubscribers().stream().anyMatch(x -> x.getId() == idUserLogged)) {
                    //((Button) cell.getGraphic()).setText("Y");
                    icon = new ImageView("checkbox-unchecked-16x16.png");
                } else {
                    //((Button) cell.getGraphic()).setText("N");
                    icon = new ImageView("checkbox-checked-16x16.png");
                }
                btn.setGraphic(icon);
            }
        }));
        evt_description.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("description"));
        evt_date.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("eventDate"));
        event_list.setItems(eventList);

        due_evt_description.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("description"));
        due_evt_date.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("eventDate"));
        due_event_list.setItems(dueEventList);
    }

    private void initialize_reports() {
        rap_de_la.setValue(LocalDate.now());
        rap_pana_la.setValue(LocalDate.now());
        Callback<ListView<Utilizator>, ListCell<Utilizator>> factory = lv -> new ListCell<Utilizator>() {
            @Override
            protected void updateItem(Utilizator item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getFirstName() + " " + item.getLastName());
            }
        };
        rap_friend.setCellFactory(factory);
        rap_friend.setButtonCell(factory.call(null));
        refresh_friends_combo();
    }

    private void refresh_friends_combo() {
        //rap_friend.getItems().clear();
        rap_friend.getItems().setAll(observableList);
    }

    private Node createFriendPage(int pageIndex) {
        int fromIndex = pageIndex * rowsPerPage;
        int toIndex = Math.min(fromIndex + rowsPerPage, observableList.size());
        if(observableList.isEmpty()) return null;
        friends_list.setItems(FXCollections.observableList(observableList.subList(fromIndex, toIndex)));
        return new BorderPane(friends_list);
    }

    private void initialize_friends_list() {
        friend_list_pagination.setPageFactory(this::createFriendPage);
        //ImageView imageDel = new ImageView("remove.png");
        friend_delete.setCellFactory(ActionButtonTableCell.<Utilizator>forTableColumn("", "remove-16x16.png", (Utilizator u) -> {
            // System.out.println(u);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Stergere prieten");
            alert.setHeaderText(u.getFirstName() + " " + u.getLastName());
            alert.setContentText("Doresti sa il stergi din lista de prieteni?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                try {
                    srv.delete_friendship(idUserLogged, u.getId());
                } catch (Exception e) {
                }
                try {
                    srv.delete_friendship(u.getId(), idUserLogged);
                } catch (Exception e) {
                }
                refresh_friend_list();
                refresh_requests();
                refresh_possible_friend();
                refresh_messages(0);
            }
            return u;
        }, null));

        friend_firstName.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        friend_lastName.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        friends_list.setItems(observableList);

        Callback<ListView<Utilizator>, ListCell<Utilizator>> factory = lv -> new ListCell<Utilizator>() {
            @Override
            protected void updateItem(Utilizator item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getFirstName() + " " + item.getLastName());
            }
        };
        friend_name.setCellFactory(factory);
        friend_name.setButtonCell(factory.call(null));
    }

    private void refresh_possible_friend() {
        if(srv == null) return;
        ArrayList<Utilizator> lista = new ArrayList<Utilizator>();
        for(Utilizator t: srv.getAll()){
            if(t.getId() != idUserLogged) {
                if(!observableList.stream().anyMatch(u->u.getId()==t.getId())) {
                    if(!requestList.stream().anyMatch(r->r.getIdUser() == t.getId()))
                        lista.add(t);
                }
            }
        }
        possibleFriendsList.setAll(lista);
        friend_name.setItems(possibleFriendsList);
    }

    private void initialize_messages_list() {
        //ImageView imageReply = new ImageView("reply.png");
        msg_reply_btn.setCellFactory(ActionButtonTableCell.<ConvDTO>forTableColumn("", "reply-16x16.png", (ConvDTO convDTO) -> {
            // System.out.println(convDTO);
            Reply(convDTO);
            return convDTO;
        }, null));

        msg_firstName.setCellValueFactory(new PropertyValueFactory<ConvDTO, String>("firstName"));
        msg_lastName.setCellValueFactory(new PropertyValueFactory<ConvDTO, String>("lastName"));
        msg_mesaj.setCellValueFactory(new PropertyValueFactory<ConvDTO, String>("mesaj"));
        msg_reply_to.setCellValueFactory(new PropertyValueFactory<ConvDTO, String>("mesajReply"));
        message_list.setItems(msgList);
    }

    private Node createRequestPage(int pageIndex) {
        int fromIndex = pageIndex * rowsPerPage;
        int toIndex = Math.min(fromIndex + rowsPerPage, requestList.size());
        if(requestList.isEmpty()) return null;
        request_list.setItems(FXCollections.observableList(requestList.subList(fromIndex, toIndex)));
        return new BorderPane(request_list);
    }

    private void initialize_request_list() {
        request_list_pagination.setPageFactory(this::createRequestPage);
        req_accept.setCellFactory(ActionButtonTableCell.<ReqUser>forTableColumn("", "accept-16x16.png", (ReqUser reqUser) -> {
            // System.out.println(reqUser);
            if ( reqUser.getStatus().equals("pending"))
            {
                Request r = srvRquest.getOne(reqUser.getIdRequest());
                srvRquest.accept_friend_request(r.getWhoRequested(),r.getWhoRecived());
                refresh_requests();
                refresh_friend_list();
                refresh_possible_friend();
            }
            return reqUser;
        }, (ReqUser reqUser, TableCell cell) -> {
            if ((reqUser.getIdRequest().getLeft() == idUserLogged) || !reqUser.getStatus().equals("pending")) {
                cell.setDisable(true);
            } else {
                cell.setDisable(false);
            }
        }));
        req_reject.setCellFactory(ActionButtonTableCell.<ReqUser>forTableColumn("", "reject-16x16.png", (ReqUser reqUser) -> {
            // System.out.println(reqUser);
            if ( reqUser.getStatus().equals("pending")) {
                Request r = srvRquest.getOne(reqUser.getIdRequest());
                srvRquest.decline_friend_request(r.getWhoRequested(), r.getWhoRecived());
                refresh_requests();
            }
            return reqUser;
        }, (ReqUser reqUser, TableCell cell) -> {
            if ((reqUser.getIdRequest().getLeft() == idUserLogged) || !reqUser.getStatus().equals("pending")) {
                cell.setDisable(true);
            } else {
                cell.setDisable(false);
            }
        }));
        req_withdraw.setCellFactory(ActionButtonTableCell.<ReqUser>forTableColumn("", "withdraw-16x16.png", (ReqUser reqUser) -> {
            // System.out.println(reqUser);
            if ( reqUser.getStatus().equals("pending")) {
                srvRquest.delete(reqUser.getIdRequest());
                refresh_requests();
                refresh_possible_friend();
            }
            return reqUser;
        }, (ReqUser reqUser, TableCell cell) -> {
            if ((reqUser.getIdRequest().getLeft() != idUserLogged) || !reqUser.getStatus().equals("pending")) {
                cell.setDisable(true);
            } else {
                cell.setDisable(false);
            }
        }));

        req_firstName.setCellValueFactory(new PropertyValueFactory<ReqUser,String>("nume"));
        req_lastName.setCellValueFactory(new PropertyValueFactory<ReqUser,String>("lastName"));
        req_status.setCellValueFactory(new PropertyValueFactory<ReqUser,String>("status"));
        req_date.setCellValueFactory(new PropertyValueFactory<ReqUser,String>("date"));
        request_list.setItems(requestList);
    }

    public void initial(UtilizatorService srvU, PrietenieService srvP, MessageService srvMessaj, RequestService srvRequest, EventService srvEvent) {
        this.idUserLogged = idUserLogged;
        this.srv = srvU;
        this.srvPrietenie = srvP;
        this.srvMessaj = srvMessaj;
        this.srvRquest = srvRequest;
        this.srvEvent = srvEvent;
        SetLoggedIn(0L);
    }

    public void initial(Long idUserLogged, UtilizatorService srvU, PrietenieService srvP, MessageService srvMessaj, RequestService srvRequest, EventService srvEvent) {
        initial(srvU, srvP, srvMessaj, srvRequest, srvEvent);
        SetLoggedIn(idUserLogged);
    }

    public void init_content(Long idUserLogged) {
        ut = srv.getOne(this.idUserLogged);
        full_name.setText(ut.getLastName() + " " + ut.getFirstName());
        refresh_all();
    }

    private void refresh_pagination(Pagination pagination, ObservableList observableList, Callback<Integer, Node> callback) {
        int crtPageIdx = pagination.getCurrentPageIndex();
        pagination.setPageCount(observableList.size() / rowsPerPage + (observableList.size() % rowsPerPage != 0 ? 1 : 0));
        if (crtPageIdx > pagination.getMaxPageIndicatorCount()) {
            crtPageIdx = pagination.getMaxPageIndicatorCount();
        }
        pagination.setPageFactory(callback);
        pagination.setCurrentPageIndex(crtPageIdx);
    }

    private void refresh_events_basic() {
        Iterable<Event> events = srvEvent.getAll();
        ArrayList<EventDTO> evs = new ArrayList<>();
        for (Event ev: events) {
            evs.add(new EventDTO(ev.getDescription(), ev.getEventDate(), ev.getOwner().getId(), ev.getId()));
        }
        eventList.setAll(evs);
        refresh_pagination(event_list_pagination, eventList, this::createEventPage);
    }

    private Page<Event> pagedEvents;

    private Node createPaginatedEventPage(int pageIndex) {
        PageableImplementation eventsPage = new PageableImplementation(pageIndex, rowsPerPage);
        pagedEvents = srvEvent.getAll(eventsPage, false);
        ArrayList<EventDTO> evs = new ArrayList<>();
        pagedEvents.getContent().forEach(ev -> evs.add(new EventDTO(ev.getDescription(), ev.getEventDate(), ev.getOwner().getId(), ev.getId())));
        if (evs.size() == 0) {
            Platform.runLater(() -> event_list_pagination.setCurrentPageIndex(pageIndex - 1));
            // return null;
        }
        else {
            eventList.setAll(evs);
        }
        // event_list.setItems(FXCollections.observableList(eventList));
        return new BorderPane(event_list);
    }

    private void refresh_events() {
        int crtPageIdx = event_list_pagination.getCurrentPageIndex();

        PageableImplementation eventsPage = new PageableImplementation(event_list_pagination.getCurrentPageIndex(), rowsPerPage);
        pagedEvents = srvEvent.getAll(eventsPage, true);
        ArrayList<EventDTO> evs = new ArrayList<>();
        pagedEvents.getContent().forEach(ev -> evs.add(new EventDTO(ev.getDescription(), ev.getEventDate(), ev.getOwner().getId(), ev.getId())));
        eventList.setAll(evs);

        event_list_pagination.setPageFactory(this::createPaginatedEventPage);
        event_list_pagination.setCurrentPageIndex(crtPageIdx);
    }

    private void refresh_dueEvents() {
        Iterable<Event> events = srvEvent.getAllSubscribedDue(idUserLogged, reminderDays);
        ArrayList<EventDTO> evs = new ArrayList<>();
        for (Event ev: events) {
            evs.add(new EventDTO(ev.getDescription(), ev.getEventDate(), ev.getOwner().getId(), ev.getId()));
        }
        dueEventList.setAll(evs);
        btn_due_events.setVisible(dueEventList.size() > 0);
        btn_due_events.setText(""+dueEventList.size());

        refresh_pagination(due_event_list_pagination, dueEventList, this::createDueEventPage);
    }

    private void refresh_requests() {
        Iterable<Request> friendRequests = srvRquest.getAll();
        Iterable<Request> friendRequestsbuni=new ArrayList<>();

        friendRequestsbuni= StreamSupport.stream(friendRequests.spliterator(), false)
                .filter(x -> x.getId().getRight().equals(idUserLogged) || x.getId().getLeft().equals(idUserLogged) )
                .collect(Collectors.toList());

        ArrayList<Tuple<Utilizator,Request>> lis = new ArrayList<Tuple<Utilizator,Request>>();
        friendRequestsbuni.forEach(x->{
            if (srv.getOne(x.getId().getLeft()).getId().equals(idUserLogged))
            {
                lis.add(new Tuple(srv.getOne(x.getId().getRight()),x));
            }
            else
            {
                lis.add(new Tuple(srv.getOne(x.getId().getLeft()),x));
            }
        });
        // System.out.println(lis);
        ArrayList<ReqUser> re = new ArrayList<ReqUser>();
        for (Tuple<Utilizator, Request> li : lis) {
            ReqUser r = new ReqUser(li.getRight().getId(),li.getLeft().getId(), li.getLeft().getFirstName(),li.getRight().getStatus(),li.getRight().getdata());
            re.add(r);
        }
        // System.out.println(re);
        requestList.setAll(re);
        refresh_pagination(request_list_pagination, requestList, this::createRequestPage);
    }

    private Node createMessagePage(int pageIndex) {
        int fromIndex = pageIndex * rowsPerPage;
        int toIndex = Math.min(fromIndex + rowsPerPage, msgList.size());
        if(msgList.isEmpty()) return null;
        message_list.setItems(FXCollections.observableList(msgList.subList(fromIndex, toIndex)));
        return new BorderPane(message_list);
    }

    private void refresh_messages(long idUserChatWith) {
        java.util.List<Message> convorbireaRaw= srvMessaj.cronologic_message(idUserLogged,idUserChatWith);
        List<ConvDTO> mesaje=new ArrayList<>();
        for(Message m: convorbireaRaw){
            if(m.getReply() == 0L)
                mesaje.add(new ConvDTO(m.get_from().getFirstName(),m.get_from().getLastName(),m.get_message(),"",m.getId()));
            else
                mesaje.add(new ConvDTO(m.get_from().getFirstName(),m.get_from().getLastName(),m.get_message(),srvMessaj.getOne(m.getReply()).get_message(),m.getId()));
        }
        msgList.setAll(mesaje);
        refresh_pagination(message_list_pagination, msgList, this::createMessagePage);
    }

    private void refresh_friend_list()
    {
        observableList.removeAll();
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        ArrayList<Utilizator> lista = new ArrayList<Utilizator>();
        for(Tuple<Utilizator, Prietenie> t: srvPrietenie.get_friends_list(idUserLogged)){
            String nume = t.getLeft().getFirstName() + " " +t.getLeft().getLastName();
            String data = t.getRight().getDate().format(formatter);
            lista.add(t.getLeft());
        }
        observableList.setAll(lista);
        refresh_pagination(friend_list_pagination, observableList, this::createFriendPage);
        // System.out.println(observableList);

        refresh_friends_combo();
    }

    public void FriendSelected(MouseEvent mouseEvent) {
        selected = (Utilizator) friends_list.getSelectionModel().getSelectedItem();
        if(selected != null) {
            refresh_messages(selected.getId());
        }
        else {
            refresh_messages(0);
        }
    }

    public void SendMsg(MouseEvent mouseEvent) {
        if (selected == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Prieten neselectat!");
            alert.setContentText("Nu poti trimite un mesaj daca nu ai selectat un prieten din lista!");
            alert.showAndWait();
            return;
        }
        String mesajDeTrimis=messageToSend.getText();
        if(mesajDeTrimis.isEmpty())
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Mesaj gol!");
            alert.setContentText("Nu poti trimite un mesaj gol!");
            alert.showAndWait();
        }
        else{
            long maxid=-1;
            for(Message m:srvMessaj.getAll()){
                if(m.getId()>maxid)
                    maxid=m.getId();
            }
            List<Long> dest=new ArrayList<>();
            dest.add(selected.getId());
            srvMessaj.send_message(maxid+1,idUserLogged,dest,mesajDeTrimis);
            messageToSend.setText("");
            refresh_messages(selected.getId());
        }
    }

    public void Reply(ConvDTO selectedMsg) {
        if(selectedMsg==null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Selectie goala!");
            alert.setContentText("Inainte de a da un reply unui mesaj, selectati un mesaj!");
            alert.showAndWait();
        }
        else
        {
            String mesajDeTrimis=messageToSend.getText();
            if(mesajDeTrimis.isEmpty())
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Mesaj gol!");
                alert.setContentText("Nu poti trimite un mesaj gol!");
                alert.showAndWait();
            }
            else{
                Message msg = srvMessaj.getOne(selectedMsg.getIdMesaj());
                if (msg.get_from().getId() == idUserLogged) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Mesaj propriu!");
                    alert.setContentText("Nu poti trimite un raspuns la un mesaj propriu!");
                    alert.showAndWait();
                }
                else {
                    long maxid = -1;
                    for (Message m : srvMessaj.getAll()) {
                        if (m.getId() > maxid)
                            maxid = m.getId();
                    }
                    srvMessaj.reply_one(maxid + 1, selectedMsg.getIdMesaj(), idUserLogged, mesajDeTrimis, msg.get_from().getId());
                    messageToSend.setText("");
                    refresh_messages(selected.getId());
                }
            }
        }
    }

    public void AddFriend(MouseEvent mouseEvent) {
        friend_add_err.setText("");
        /*String name = ((Utilizator)friend_name.getValue()).getFirstName();
        if(name.isEmpty()){
            friend_add_err.setText("Field is empty");
            return;
        }*/
        Utilizator user_cautat = (Utilizator)friend_name.getValue();
        /*for(Utilizator u: srv.getAll()){
            if(u.getFirstName().equals(name))
                user_cautat=u;
        }*/
        if(user_cautat==null)
        {
            friend_add_err.setText("This user does not exists");
            return;
        }

        for(Tuple<Utilizator,Prietenie> t: srvPrietenie.get_friends_list(idUserLogged)){

            if (t.getLeft().equals(user_cautat)) {
                friend_add_err.setText("This friend already exists");
                return;
            }
        }
        try{
            srvRquest.send_request(idUserLogged,user_cautat.getId());
            friend_name.setValue(null);
            refresh_friend_list();
            refresh_requests();
            refresh_possible_friend();
        } catch(Exception ex){

        }
    }

    private String SelectPDFPath(String title, String defaultFileName) {
        // Stage stage = (Stage)((Node) mouseEvent.getSource()).getScene().getWindow();
        Stage stage = (Stage)page.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PDF Files", "*.pdf")
        );
        // fileChooser.setInitialDirectory(new File("C:/Temp"));
        fileChooser.setInitialFileName(defaultFileName);
        File selectedFile = fileChooser.showSaveDialog(stage);
        return selectedFile == null ? null : selectedFile.getPath();
    }

    private Document InitializePDFDocument(String destination) {
        // Generez documentul PDF
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(destination);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eroare raport!");
            alert.setContentText("Nu se poate crea fisierul pentru raportul ales!");
            alert.showAndWait();
            return null;
        }
        PdfWriter writer = new PdfWriter(fos);
        // Initialize PDF document
        PdfDocument pdf = new PdfDocument(writer);
        // Initialize document
        return new Document(pdf);
    }

    public void UserActivityReport(MouseEvent mouseEvent) {
        String destination = SelectPDFPath("Specificati fisierul in care se va salva raportul de activitate a utilizatorului","UserActivityReport.pdf");
        if(destination==null) return;
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String str_de_la = rap_de_la.getValue().format(formatter);
        String str_pana_la = rap_pana_la.getValue().format(formatter);

        // Lista de prieteni pentru perioada specificate
        ArrayList<Utilizator> lista = new ArrayList<Utilizator>();
        for(Tuple<Utilizator, Prietenie> t: srvPrietenie.get_friends_list(idUserLogged)){
            String nume = t.getLeft().getFirstName() + " " +t.getLeft().getLastName();
            String data = t.getRight().getDate().format(formatter);
            if (data.compareTo(str_de_la) >= 0 && data.compareTo(str_pana_la) <= 0) {
                lista.add(t.getLeft());
            }
        }

        // Lista de mesaje primite pentru perioada specificate
        List<ConvDTO> mesaje=new ArrayList<>();
        for(Message m: srvMessaj.getAll()) {
            if (!m.get_to().stream().anyMatch(u->u.getId()==idUserLogged))
                continue;
            String data = m.getData().format(formatter);
            if (data.compareTo(str_de_la) >= 0 && data.compareTo(str_pana_la) <= 0) {
                if (m.getReply() == 0L)
                    mesaje.add(new ConvDTO(m.get_from().getFirstName(), m.get_from().getLastName(), m.get_message(), "", m.getId()));
                else
                    mesaje.add(new ConvDTO(m.get_from().getFirstName(), m.get_from().getLastName(), m.get_message(), srvMessaj.getOne(m.getReply()).get_message(), m.getId()));
            }
        }

        Document document = InitializePDFDocument(destination);
        if (document == null) {
            return;
        }

        // Add paragraphs to the document
        document.add(new Paragraph("Lista de prieteni pentru perioada: " + str_de_la + " - " + str_pana_la));
        for (Utilizator u: lista) {
            document.add(new Paragraph(u.getFirstName() + " " + u.getLastName()));
        }
        document.add(new Paragraph(""));
        document.add(new Paragraph("Lista de mesaje primite pentru perioada: " + str_de_la + " - " + str_pana_la));
        for (ConvDTO convDTO: mesaje) {
            document.add(new Paragraph( convDTO.getFirstName() + " " + convDTO.getLastName() + " : " + convDTO.getMesaj() +
                    (convDTO.getMesajReply().isEmpty() ? "" : " (" + convDTO.getMesajReply() + ")")
            ));
        }

        // Close document
        document.close();
    }

    public void MessagesReceivedReport(MouseEvent mouseEvent) {
        Utilizator friend = (Utilizator)rap_friend.getValue();
        if (friend == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Alege un prieten!");
            alert.setContentText("Pentru a genera raportul trebuie sa alegi un prieten!");
            alert.showAndWait();
            return;
        }

        String destination = SelectPDFPath("Specificati fisierul in care se va salva raportul de mesaje primite de la un prieten","MessagesReceivedReport.pdf");
        if(destination == null) return;
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String str_de_la = rap_de_la.getValue().format(formatter);
        String str_pana_la = rap_pana_la.getValue().format(formatter);

        // Lista de mesaje primite de la prieten pentru perioada specificata
        List<ConvDTO> mesaje=new ArrayList<>();
        for(Message m: srvMessaj.getAll()) {
            if (m.get_from().getId() != friend.getId())
                continue;;
            if (m.get_to().stream().filter(u->u.getId()==idUserLogged).findFirst() == null)
                continue;
            String data = m.getData().format(formatter);
            if (data.compareTo(str_de_la) >= 0 && data.compareTo(str_pana_la) <= 0) {
                if (m.getReply() == 0L)
                    mesaje.add(new ConvDTO(m.get_from().getFirstName(), m.get_from().getLastName(), m.get_message(), "", m.getId()));
                else
                    mesaje.add(new ConvDTO(m.get_from().getFirstName(), m.get_from().getLastName(), m.get_message(), srvMessaj.getOne(m.getReply()).get_message(), m.getId()));
            }
        }

        Document document = InitializePDFDocument(destination);
        if (document == null) {
            return;
        }

        // Add paragraphs to the document
        document.add(new Paragraph("Lista de mesaje primite de la " + friend.getFirstName() + " " + friend.getLastName() + " pentru perioada: " + str_de_la + " - " + str_pana_la));
        for (ConvDTO convDTO: mesaje) {
            document.add(new Paragraph( convDTO.getFirstName() + " " + convDTO.getLastName() + " : " + convDTO.getMesaj() +
                    (convDTO.getMesajReply().isEmpty() ? "" : " (" + convDTO.getMesajReply() + ")")
            ));
        }

        // Close document
        document.close();
    }

    public void AddEvent(MouseEvent mouseEvent) {
        Utilizator owner = srv.getOne(idUserLogged);
        String description = event_description.getText();
        LocalDateTime eventDate = LocalDateTime.of(event_date.getValue(), LocalTime.MIN);
        Event event = new Event(owner, description, new ArrayList<Utilizator>(), eventDate);
        long maxid=-1;
        for(Event e:srvEvent.getAll()){
            if(e.getId()>maxid)
                maxid=e.getId();
        }
        event.setId(maxid+1);
        srvEvent.addEvent(event);
        refresh_events();
    }

    public void login() {
        erroruser.setText("");
        errorpassword.setText("");

        String u = user_field.getText();
        String pass = password_field.getText();
        if(u.isEmpty() || pass.isEmpty())
            errorpassword.setText("Nu puteti lasa nici un camp necompletat!");

        String sha256hex = Hashing.sha256()
                .hashString(pass + u.toLowerCase(), StandardCharsets.UTF_8)
                .toString();
        pass = sha256hex;

        System.out.println("User: " + u);
        System.out.println("Hash: " + pass);

        Utilizator ut= null;
        Iterable<Utilizator> lis = srv.getAll();
        for (Utilizator utilizator : lis) {
            if(utilizator.getusername().equals(u) && utilizator.getpassword().equals(pass))
                ut = utilizator;
        }
        if(ut==null){
            erroruser.setText("Invalid account");
            return;
        }
        SetLoggedIn(ut.getId());
    }

    public void show_events() {
        accord.setExpandedPane(events_pane);
    }

    public void logout() {
        SetLoggedIn(0L);
    }

    private void SetLoggedIn(Long id) {
        idUserLogged = id;
        isLoggedIn = idUserLogged != 0;
        page_login.setVisible(!isLoggedIn);
        page_content.setVisible((isLoggedIn));
        if (isLoggedIn) {
            init_content(idUserLogged);
            dataRefresher.play();
        } else {
            dataRefresher.stop();
            Platform.runLater(() -> user_field.requestFocus());
        }
    }

    public void register() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/Views/Register.fxml"));
        Pane root;
        root = loader.load();

        Stage register_stage=new Stage();
        RegisterController regCtr = loader.getController();
        regCtr.initial(this.srv,this.srvPrietenie,this.srvMessaj,this.srvRquest);
        register_stage.setScene(new Scene(root));
        register_stage.show();
    }

    public void refresh_all(){
        refresh_events();
        refresh_dueEvents();
        refresh_friend_list();
        refresh_requests();
        refresh_possible_friend();
        FriendSelected(null);
    }

    public void settings() {
        Dialog<List<String>> dialog = new Dialog<>();
        dialog.setTitle("Setari");
        dialog.setHeaderText("Diverse setari");
        ImageView logo = new ImageView("cog.png");
        logo.setFitHeight(32);
        logo.setFitWidth(32);
        dialog.setGraphic(logo);

        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

        TextField reminderDaysField = new TextField();
        reminderDaysField.setPromptText("Numar zile amintire");
        reminderDaysField.setText(reminderDays.toString());
        TextField rowsPerPageField = new TextField();
        rowsPerPageField.setPromptText("Linii pe pagina");
        rowsPerPageField.setText(rowsPerPage.toString());
        TextField dataRefreshPeriodField = new TextField();
        dataRefreshPeriodField.setPromptText("Secunde actualizare");
        dataRefreshPeriodField.setText(dataRefreshPeriod.toString());
        CheckBox autoRefreshField = new CheckBox();
        autoRefreshField.setSelected(autoRefresh);

        grid.add(new Label("Numar zile:"), 0, 0);
        grid.add(reminderDaysField, 1, 0);
        grid.add(new Label("Linii pe pagina:"), 0, 1);
        grid.add(rowsPerPageField, 1, 1);
        grid.add(new Label("Secunde actualizare:"), 0, 2);
        grid.add(dataRefreshPeriodField, 1, 2);
        grid.add(new Label("Actualizare automata:"), 0, 3);
        grid.add(autoRefreshField, 1, 3);

        dialog.getDialogPane().setContent(grid);
        Platform.runLater(() -> reminderDaysField.requestFocus());

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == ButtonType.OK) {
                List<String> list = new ArrayList<>();
                list.add(reminderDaysField.getText());
                list.add(rowsPerPageField.getText());
                list.add(dataRefreshPeriodField.getText());
                list.add(autoRefreshField.isSelected() ? "1" : "0");
                return list;
            }
            return null;
        });

        Optional<List<String>> result = dialog.showAndWait();
        Integer oldReminderDays = reminderDays;
        Integer oldRowsPerPage = rowsPerPage;
        Integer oldDataRefreshPeriod = dataRefreshPeriod;
        Boolean oldAutoRefresh = autoRefresh;
        result.ifPresent(res -> {
            reminderDays = Integer.parseInt(res.get(0));
            rowsPerPage = Integer.parseInt(res.get(1));
            dataRefreshPeriod = Integer.parseInt(res.get(2));
            autoRefresh = Integer.parseInt(res.get(3)) == 1;
        });
        if ((oldReminderDays != reminderDays) || (oldRowsPerPage != rowsPerPage)) {
            refresh_dueEvents();
        }
        if (oldRowsPerPage != rowsPerPage) {
            refresh_all();
        }
        if (oldDataRefreshPeriod != dataRefreshPeriod) {
            dataRefresher.stop();
            initialize_data_refresher();
        }
    }
}
